#ifndef USERCONNECTIONDATAHASH_H
#define USERCONNECTIONDATAHASH_H

#include <QObject>

#include <QTcpSocket>
#include <QPointer>

class UserConnectionDataHash : public QObject
{
    Q_OBJECT
public:
    explicit UserConnectionDataHash(const int &userID,
                                    const QString &userToken,
                                    const QString &userLogin,
                                    QTcpSocket *clientSocket,
                                    QObject *parent = nullptr);

    void setUserID(const int &userID);
    int getUserID();

    void setUserToken(const QString &userToken);
    QString getUserToken();

    void setUserLogin(const QString &userLogin);
    QString getUserLogin();

    void setClientSocket(QTcpSocket *clientSocket);
    QTcpSocket *getClientSocket();

private:
    int m_userID;
    int m_clientDescriptor;
    QString m_userToken;
    QString m_userLogin;
    QPointer<QTcpSocket> m_clientSocket;
};

#endif // USERCONNECTIONDATAHASH_H
