#include "userconnectiondatahash.h"

UserConnectionDataHash::UserConnectionDataHash(const int &userID,
                                               const QString &userToken,
                                               const QString &userLogin,
                                               QTcpSocket *clientSocket,
                                               QObject *parent) : QObject(parent)
{
    m_userID = userID;
    m_userToken = userToken;
    m_userLogin = userLogin;

    m_clientSocket = new QTcpSocket();
    m_clientSocket = clientSocket;
}

void UserConnectionDataHash::setUserID(const int &userID)
{
    m_userID = userID;
}

int UserConnectionDataHash::getUserID()
{
    return m_userID;
}

void UserConnectionDataHash::setUserToken(const QString &userToken)
{
    m_userToken = userToken;
}

QString UserConnectionDataHash::getUserToken()
{
    return m_userToken;
}

void UserConnectionDataHash::setUserLogin(const QString &userLogin)
{
    m_userLogin = userLogin;
}

QString UserConnectionDataHash::getUserLogin()
{
    return m_userToken;
}

void UserConnectionDataHash::setClientSocket(QTcpSocket *clientSocket)
{
    if (!m_clientSocket) {
        m_clientSocket = new QTcpSocket();
    }

    m_clientSocket = clientSocket;
}

QTcpSocket *UserConnectionDataHash::getClientSocket()
{
    return m_clientSocket;
}
