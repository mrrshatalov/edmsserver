#include "databasehandler.h"

DatabaseHandler::DatabaseHandler(QObject *parent) : QObject(parent)
{
    m_dbConnectionString = "DRIVER={SQL Server};SERVER=.\\SQLEXPRESS;DATABASE=EDMSBase;Trusted_Connection=yes;";
}

void DatabaseHandler::initialization()
{
    m_db = QSqlDatabase::addDatabase("QODBC");
    m_db.setDatabaseName(m_dbConnectionString);
    m_db.open();

    if (m_db.isOpen()) {
        printf("database is open \n");
    }
    else {
        printf("error - open database \n"); // << m_db.lastError().text());
    }
}

DatabaseHandler::~DatabaseHandler()
{
    if (m_db.isOpen()) {
        m_db.close();
    }
}

//Проверка наличия пользователя
int DatabaseHandler::isUserRegistered(const QString &userLogin,
                                      const QString &userPassword)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT userID "
                            "FROM EDMSBase.dbo.[User] "
                            "WHERE userLogin = '%1' AND userPasswordHash = '%2' ")
            .arg(userLogin)
            .arg(userPassword);
    int userID = -1;
    if (q.exec(query)) {
        while (q.next()) {
            userID = q.value(0).toInt();
        }

        return userID;
    }
    else {
        return -1;
    }
}

//Проверка наличия пользователя (по токену)
int DatabaseHandler::isUserRegisteredByToken(const QString &userToken)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT userID "
                            "FROM EDMSBase.dbo.[User] "
                            "WHERE userToken = '%1' ")
            .arg(userToken);

    int userID = -1;
    if (q.exec(query)) {
        while (q.next()) {
            userID = q.value(0).toInt();
        }

        return userID;
    }
    else {
        return -1;
    }
}

//Установить токен пользователя
bool DatabaseHandler::setUserToken(const int &userID,
                                   const QString &userToken,
                                   const int userDescriptor)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("UPDATE EDMSBase.dbo.[User] "
                            "SET userToken = '%2', "
                            "userDescriptor = %3 "
                            "WHERE userID = %1 ")
            .arg(userID)
            .arg(userToken)
            .arg(userDescriptor);
    return q.exec(query);
}

//Получить данные пользователя
QStringList DatabaseHandler::getUserData(const int &userID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT "
                            "userLogin, "
                            "userName, "
                            "userSurname, "
                            "userPostName "
                            "FROM EDMSBase.dbo.[User], EDMSBase.dbo.UserPost "
                            "WHERE userID = %1 AND ([User].userPostID = userPost.userPostID) ")
            .arg(userID);
    if (q.exec(query)) {
        QStringList result;
        result.clear();

        while (q.next()) {
            result.append(q.value(0).toString().trimmed());
            result.append(q.value(1).toString().replace("  ", ""));
            result.append(q.value(2).toString().replace("  ", ""));
            result.append(q.value(3).toString().replace("  ", ""));
        }

        return result;
    }
    else {
        return QStringList();
    }
}

//Получить userID по токену пользователя
int DatabaseHandler::getUserID(const QString &userToken)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = -1;
    QString query = QString("SELECT userID "
                            "FROM EDMSBase.dbo.[User] "
                            "WHERE LTRIM(RTRIM(userToken)) = '%1' ")
            .arg(userToken);
    if (q.exec(query)) {
        while (q.next()) {
            userID = q.value(0).toInt();
        }

        return userID;
    }
    else {
        return userID;
    }
}

//Получить список пользователей (кроме себя)
QString DatabaseHandler::getUserList(const QString &userToken)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT userID, "
                            "userName, "
                            "userSurname, "
                            "userPostName "
                            "FROM EDMSBase.dbo.[User], EDMSBase.dbo.UserPost "
                            "WHERE ([User].userPostID = UserPost.userPostID) AND (userToken != '%1') ")
            .arg(userToken);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("userID", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toInt())));
            jsonObj.insert("userName", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("userSurname", QJsonValue::fromVariant(q.value(2).toString()));
            jsonObj.insert("userPostName", QJsonValue::fromVariant(q.value(3).toString()));

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Создание нового документа
bool DatabaseHandler::createNewDocument(const QString &userToken,
                                        const QString &documentName,
                                        const QString &documentShortDescription,
                                        const QString &documentData,
                                        const QString &chooseUsersList,
                                        const QString &documentFormat)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QStringList chooseUsers = chooseUsersList.split(",");
    if (chooseUsers.length() != 0) {
        QSqlQuery q(m_db);
        bool z_ok = true;

        int textDocumentID = -1;
        QDateTime currentDateTime = QDateTime::currentDateTime();

        QString query = QString("INSERT INTO EDMSBase.dbo.TextDocument "
                                "(textDocumentName, "
                                "textDocumentShortDescription, "
                                "textDocumentDocument, "
                                "textDocumentStatusID, "
                                "textDocumentDateTime,"
                                "textDocumentDocumentFormat) "
                                "VALUES ('%1', '%2', '%3', 1, '%4', '%5') ")
                .arg(documentName)
                .arg(documentShortDescription)
                .arg(documentData)
                .arg(currentDateTime.toString("yyyy-MM-dd hh:mm:ss"))
                .arg(documentFormat);

        if (q.exec(query)) {
            query = QString("SELECT textDocumentID FROM EDMSBase.dbo.TextDocument "
                            "WHERE (textDocumentName = '%1') AND "
                            "(textDocumentShortDescription = '%2') AND "
                            "(textDocumentDocument = '%3') AND "
                            "(textDocumentStatusID = 1) AND "
                            "(textDocumentDateTime = '%4') AND "
                            "(textDocumentDocumentFormat = '%5') ")
                    .arg(documentName)
                    .arg(documentShortDescription)
                    .arg(documentData)
                    .arg(currentDateTime.toString("yyyy-MM-dd hh:mm:ss"))
                    .arg(documentFormat);

            if (q.exec(query)) {
                while (q.next()) {
                    textDocumentID = q.value(0).toInt();
                }

                if (textDocumentID != -1) {
                    int userID = getUserID(userToken);
                    if (userID != -1) {
                        for(int i = 0; i < chooseUsers.length(); i++) {
                            if (chooseUsers.at(i) != "") {
                                query = QString("INSERT INTO EDMSBase.dbo.UserTextDocument "
                                                "(textDocumentID, "
                                                "userTextDocumentOfferUserID, "
                                                "userTextDocumentUserID, "
                                                "userTextDocumentDateTime) "
                                                "VALUES (%1, %2, %3, GETDATE()) ")
                                        .arg(textDocumentID)
                                        .arg(userID)
                                        .arg(chooseUsers.at(i));
                                z_ok = q.exec(query);
                            }
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

//Получить список дескрипторов пользователей
QStringList DatabaseHandler::getUsersDescriptor(const QString &usersID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT userDescriptor "
                            "FROM [EDMSBase].[dbo].[User] "
                            "WHERE (userID IN (%1)) AND (userDescriptor IS NOT NULL) ")
            .arg(usersID);

    if (q.exec(query)) {
        QStringList result;

        while (q.next()) {
            result.append(QString::number(q.value(0).toInt()));
        }

        return result;
    }
    else {
        return QStringList();
    }
}

//Получить список моих документов (созданных)
QString DatabaseHandler::getMyDocumentList(const QString &userToken)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = getUserID(userToken);
    QString query = QString("SELECT textDocumentID, "
                            "textDocumentName, "
                            "textDocumentShortDescription, "
                            "textDocumentStatusName, "
                            "TextDocument.textDocumentStatusID, "
                            "textDocumentDateTime "
                            "FROM EDMSBase.dbo.TextDocument, EDMSBase.dbo.TextDocumentStatus "
                            "WHERE (textDocumentID IN (SELECT textDocumentID FROM EDMSBase.dbo.UserTextDocument WHERE userTextDocumentOfferUserID = %1)) AND "
                            "(TextDocument.textDocumentStatusID = TextDocumentStatus.textDocumentStatusID) "
                            "ORDER BY textDocumentID DESC ")
            .arg(userID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("textDocumentID", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toInt())));
            jsonObj.insert("textDocumentName", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("textDocumentShortDescription", QJsonValue::fromVariant(q.value(2).toString()));
            jsonObj.insert("textDocumentStatusName", QJsonValue::fromVariant(q.value(3).toString()));
            jsonObj.insert("textDocumentStatusID", QJsonValue::fromVariant(q.value(4).toInt()));
            jsonObj.insert("textDocumentDateTime", QJsonValue::fromVariant(q.value(5).toString()));

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Получить список моих документов (подпись)
QString DatabaseHandler::getMyDocumentSignatureList(const QString &userToken)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QSqlQuery q_ext(m_db);
    int userID = getUserID(userToken);
    QString query = QString("SELECT DISTINCT TextDocument.textDocumentID, "
                            "textDocumentName, "
                            "textDocumentShortDescription, "
                            "TextDocument.textDocumentStatusID, "
                            "textDocumentStatusName, "
                            "textDocumentDateTime, "
                            "userName, "
                            "userSurname, "
                            "UserTextDocument.userTextDocumentOfferUserID "
                            "FROM EDMSBase.dbo.TextDocument, "
                            "EDMSBase.dbo.TextDocumentStatus, "
                            "EDMSBase.dbo.UserTextDocument, "
                            "EDMSBase.dbo.[User] "
                            "WHERE (TextDocument.textDocumentID IN (SELECT DISTINCT textDocumentID FROM EDMSBase.dbo.UserTextDocument WHERE userTextDocumentUserID = %1)) AND "
                            "(TextDocument.textDocumentStatusID = TextDocumentStatus.textDocumentStatusID) AND "
                            "(TextDocument.textDocumentID = UserTextDocument.textDocumentID) AND "
                            "(UserTextDocument.userTextDocumentOfferUserID = [User].userID) "
                            "ORDER BY TextDocument.textDocumentID DESC ")
            .arg(userID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("textDocumentID", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toInt())));
            jsonObj.insert("textDocumentName", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("textDocumentShortDescription", QJsonValue::fromVariant(q.value(2).toString()));
            jsonObj.insert("textDocumentStatusID", QJsonValue::fromVariant(QString("%1").arg(q.value(3).toInt())));
            jsonObj.insert("textDocumentStatusName", QJsonValue::fromVariant(q.value(4).toString()));
            jsonObj.insert("textDocumentDateTime", QJsonValue::fromVariant(q.value(5).toString()));
            jsonObj.insert("userName", QJsonValue::fromVariant(QString("%1").arg(q.value(6).toString())));
            jsonObj.insert("userSurname", QJsonValue::fromVariant(QString("%1").arg(q.value(7).toString())));
            jsonObj.insert("userTextDocumentOfferUserID", QJsonValue::fromVariant(QString("%1").arg(q.value(8).toInt())));

            query = QString("SELECT TOP 1 "
                            "TextDocumentStage.textDocumentStageStatusID, "
                            "textDocumentStageStatusName "
                            "FROM EDMSBase.dbo.TextDocumentStage, EDMSBase.dbo.TextDocumentStageStatus "
                            "WHERE (textDocumentID = %1) AND "
                            "(userID = %2) AND "
                            "(TextDocumentStage.textDocumentStageStatusID = TextDocumentStageStatus.textDocumentStageStatusID) "
                            "ORDER BY textDocumentStageID DESC ")
                    .arg(q.value(0).toInt())
                    .arg(userID);
            if (q_ext.exec(query)) {
                if (q_ext.numRowsAffected() != 0) {
                    while (q_ext.next()) {
                        jsonObj.insert("textDocumentStageLastStatusID", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(0).toInt())));
                        jsonObj.insert("textDocumentStageStatusName", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(1).toString())));
                    }
                }
                else {
                    jsonObj.insert("textDocumentStageLastStatusID", QJsonValue::fromVariant(""));
                    jsonObj.insert("textDocumentStageStatusName", QJsonValue::fromVariant(""));
                }
            }
            else {
                return QString();
            }

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Отклонить документ
bool DatabaseHandler::rejectDocument(const QString &userToken,
                                     const int &documentID,
                                     const QString &comment)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = getUserID(userToken);
    QString query = QString("INSERT INTO EDMSBase.dbo.TextDocumentStage "
                            "(textDocumentID, "
                            "userID, "
                            "userComment, "
                            "textDocumentStageStatusID, "
                            "textDocumentStageStatusDateTime) "
                            "VALUES (%1, %2, '%3', 2, GETDATE()) ")
            .arg(documentID)
            .arg(userID)
            .arg(comment);
    if (q.exec(query)) {
        query = QString("UPDATE EDMSBase.dbo.TextDocument "
                        "SET textDocumentStatusID = 4 "
                        "WHERE textDocumentID = %1 ")
                .arg(documentID);
        return q.exec(query);
    }
    else {
        return false;
    }
}

//Получить историю документа
QString DatabaseHandler::getDocumentHistory(const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT textDocumentStageID, "
                            "TextDocumentStageStatus.textDocumentStageStatusName, "
                            "userComment, "
                            "textDocumentStageStatusDateTime, "
                            "userName, "
                            "userSurname, "
                            "TextDocumentStage.userID, "
                            "case when textDocumentDocument is not null then 1 else 0 end AS isTextDocumentThere "
                            "FROM EDMSBase.dbo.TextDocumentStage, EDMSBase.dbo.TextDocumentStageStatus, EDMSBase.dbo.[User] "
                            "WHERE textDocumentID = %1 AND "
                            "(TextDocumentStage.textDocumentStageStatusID = TextDocumentStageStatus.textDocumentStageStatusID) AND "
                            "(TextDocumentStage.userID = [User].userID) ")
            .arg(documentID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("textDocumentStageID", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toInt())));
            jsonObj.insert("textDocumentStageStatusName", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("userComment", QJsonValue::fromVariant(QString("%1").arg(q.value(2).toString())));
            jsonObj.insert("textDocumentStageStatusDateTime", QJsonValue::fromVariant(q.value(3).toString()));
            jsonObj.insert("userName", QJsonValue::fromVariant(QString("%1").arg(q.value(4).toString())));
            jsonObj.insert("userSurname", QJsonValue::fromVariant(q.value(5).toString()));
            jsonObj.insert("userID", QJsonValue::fromVariant(QString("%1").arg(q.value(6).toInt())));
            jsonObj.insert("isTextDocumentThere", QJsonValue::fromVariant(QString("%1").arg(q.value(7).toInt())));

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Уточнение по документу
bool DatabaseHandler::commentDocument(const QString &userToken,
                                      const int &documentID,
                                      const QString &comment)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query;
    int userID = getUserID(userToken);
    int documentOfferUserID = getDocumentOfferUserID(documentID);

    if (documentOfferUserID != 0) {
        if (userID != documentOfferUserID) {
            int documentOperationCount = getDocumentOperationCount(documentID,
                                                                   documentOfferUserID);

            bool isOk = true;
            if (documentOperationCount != -1) {
                if (documentOperationCount == 0) {
                    query = QString("UPDATE EDMSBase.dbo.TextDocument "
                                    "SET textDocumentStatusID = 2 "
                                    "WHERE textDocumentID = %1 ")
                            .arg(documentID);
                    if (q.exec(query)) {
                        isOk = true;
                    }
                    else {
                        return false;
                    }
                }

                if (isOk) {
                    query = QString("INSERT INTO EDMSBase.dbo.TextDocumentStage "
                                            "(textDocumentID, "
                                            "userID, "
                                            "userComment, "
                                            "textDocumentStageStatusID, "
                                            "textDocumentStageStatusDateTime) "
                                            "VALUES (%1, %2, '%3', 4, GETDATE()) ")
                            .arg(documentID)
                            .arg(userID)
                            .arg(comment);
                    return q.exec(query);
                }
            }
            else {
                return false;
            }
        }
        else {
            query = QString("INSERT INTO EDMSBase.dbo.TextDocumentStage "
                                    "(textDocumentID, "
                                    "userID, "
                                    "userComment, "
                                    "textDocumentStageStatusID, "
                                    "textDocumentStageStatusDateTime) "
                                    "VALUES (%1, %2, '%3', 4, GETDATE()) ")
                    .arg(documentID)
                    .arg(userID)
                    .arg(comment);
            return q.exec(query);
        }
    }
    else {
        return false;
    }
}

//Принятие документа
bool DatabaseHandler::acceptDocument(const QString &userToken,
                                     const int &documentID,
                                     const QString &comment)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = getUserID(userToken);
    QString query;

    int documentUserCount = getDocumentUserCount(documentID);
    if (documentUserCount != 0) {
        query = QString("INSERT INTO EDMSBase.dbo.TextDocumentStage "
                        "(textDocumentID, "
                        "userID, "
                        "userComment, "
                        "textDocumentStageStatusID, "
                        "textDocumentStageStatusDateTime) "
                        "VALUES (%1, %2, '%3', 1, GETDATE()) ")
        .arg(documentID)
        .arg(userID)
        .arg(comment);

        if (q.exec(query)) {
            int documentUserAcceptCount = getDocumentUserAcceptCount(documentID);
            if (documentUserAcceptCount != -1) {
                if (documentUserAcceptCount == documentUserCount) {
                    query = QString("UPDATE EDMSBase.dbo.TextDocument "
                                    "SET textDocumentStatusID = 3 "
                                    "WHERE textDocumentID = %1 ")
                            .arg(documentID);
                    return q.exec(query);
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

//Получить ID пользователя, создавшего документ
int DatabaseHandler::getDocumentOfferUserID(const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = -1;
    QString query = QString("SELECT userTextDocumentOfferUserID "
                            "FROM EDMSBase.dbo.UserTextDocument "
                            "WHERE textDocumentID = %1 ")
            .arg(documentID);
    if (q.exec(query)) {
        while (q.next()) {
            userID = q.value(0).toInt();
        }

        return userID;
    }
    else {
        return userID;
    }
}

//Получить список пользователей по документу (кроме себя)
QList<int> DatabaseHandler::getDocumentUsers(const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT userTextDocumentUserID "
                            "FROM EDMSBase.dbo.UserTextDocument "
                            "WHERE textDocumentID = %1 ")
            .arg(documentID);
    if (q.exec(query)) {
        QList<int> userIDs;
        userIDs.clear();

        while (q.next()) {
            userIDs.append(q.value(0).toInt());
        }

        return userIDs;
    }
    else {
        return QList<int>();
    }
}

//Скачать документ
QString DatabaseHandler::downloadDocument(const QString &userToken,
                                          const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QString query = QString("SELECT textDocumentDocument, "
                            "textDocumentDocumentFormat "
                            "FROM EDMSBase.dbo.TextDocument "
                            "WHERE textDocumentID = %1 ")
            .arg(documentID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("textDocumentDocument", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toString())));
            jsonObj.insert("textDocumentDocumentFormat", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Получить список сотрудников со статусом документа по каждому сотруднику
QString DatabaseHandler::getUsersDocumentStageData(const QString &userToken,
                                                   const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    QSqlQuery q_ext(m_db);

    int offerUserID = getUserID(userToken);
    int userID = 0;
    QString query = QString("SELECT "
                            "userTextDocumentUserID, "
                            "userName, userSurname "
                            "FROM EDMSBase.dbo.UserTextDocument, EDMSBase.dbo.[User] "
                            "WHERE (userTextDocumentOfferUserID = %1) AND "
                            "(textDocumentID = %2) AND "
                            "(UserTextDocument.userTextDocumentUserID = [User].userID) ")
            .arg(offerUserID)
            .arg(documentID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            userID = q.value(0).toInt();
            jsonObj.insert("userID", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toInt())));
            jsonObj.insert("userName", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("userSurname", QJsonValue::fromVariant(QString("%1").arg(q.value(2).toString())));

            query = QString("SELECT TOP 1 "
                            "userComment, "
                            "TextDocumentStage.textDocumentStageStatusID, "
                            "textDocumentStageStatusName, "
                            "textDocumentStageStatusDateTime "
                            "FROM EDMSBase.dbo.TextDocumentStage, EDMSBase.dbo.TextDocumentStageStatus "
                            "WHERE (textDocumentID = %1) AND "
                            "(userID = %2) AND "
                            "(TextDocumentStage.textDocumentStageStatusID = TextDocumentStageStatus.textDocumentStageStatusID) "
                            "ORDER BY textDocumentStageStatusDateTime DESC ")
                    .arg(documentID)
                    .arg(userID);

            if (q_ext.exec(query)) {
                if (q_ext.numRowsAffected() != 0) {
                    while (q_ext.next()) {
                        jsonObj.insert("userComment", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(0).toString())));
                        jsonObj.insert("textDocumentStageStatusID", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(1).toInt())));
                        jsonObj.insert("textDocumentStageStatusName", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(2).toString())));
                        jsonObj.insert("textDocumentStageStatusDateTime", QJsonValue::fromVariant(QString("%1").arg(q_ext.value(3).toString())));
                    }
                }
                else {
                    jsonObj.insert("userComment", QJsonValue::fromVariant(""));
                    jsonObj.insert("textDocumentStageStatusID", QJsonValue::fromVariant(""));
                    jsonObj.insert("textDocumentStageStatusName", QJsonValue::fromVariant(""));
                    jsonObj.insert("textDocumentStageStatusDateTime", QJsonValue::fromVariant(""));
                }
            }
            else {
                return QString();
            }

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Загрузить документ
bool DatabaseHandler::uploadDocument(const QString &userToken,
                                     const int &documentID,
                                     const QString &comment,
                                     const QString &documentData,
                                     const QString &documentFormat)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userID = getUserID(userToken);
    QString query = QString("INSERT INTO EDMSBase.dbo.TextDocumentStage "
                            "(textDocumentID, "
                            "userID, "
                            "userComment, "
                            "textDocumentStageStatusID, "
                            "textDocumentStageStatusDateTime, "
                            "textDocumentDocument, "
                            "textDocumentFormat) "
                            "VALUES (%1, %2, '%3', 4, GETDATE(), '%4', '%5')  ")
            .arg(documentID)
            .arg(userID)
            .arg(comment)
            .arg(documentData)
            .arg(documentFormat);
    if (q.exec(query)) {
        query = QString("UPDATE EDMSBase.dbo.TextDocument "
                        "SET textDocumentDocument = '%1', "
                        "textDocumentDocumentFormat = '%2' "
                        "WHERE textDocumentID = %3 ")
                .arg(documentData)
                .arg(documentFormat)
                .arg(documentID);
        return q.exec(query);
    }
    else {
        return false;
    }
}

//Скачать документ (по стадии готовности)
QString DatabaseHandler::downloadDocumentByStage(const QString &userToken,
                                                 const int &userDocumentUploaderID,
                                                 const int &documentID,
                                                 const int &documentStageID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
//    int userID = getUserID(userToken);
    QString query = QString("SELECT textDocumentDocument, "
                            "textDocumentFormat, "
                            "textDocumentStageStatusDateTime "
                            "FROM EDMSBase.dbo.TextDocumentStage "
                            "WHERE (textDocumentStageID = %1) AND "
                            "(textDocumentID = %2) AND "
                            "(userID IN (SELECT userTextDocumentOfferUserID FROM EDMSBase.dbo.UserTextDocument WHERE (userTextDocumentOfferUserID = %3) AND (textDocumentID = %2))) ")
            .arg(documentStageID)
            .arg(documentID)
            .arg(userDocumentUploaderID);

    if (q.exec(query)) {
        QJsonDocument jsonDoc;
        QJsonArray jsonArr;
        QJsonObject jsonObj;

        while (q.next()) {
            jsonObj.insert("textDocumentDocument", QJsonValue::fromVariant(QString("%1").arg(q.value(0).toString())));
            jsonObj.insert("textDocumentDocumentFormat", QJsonValue::fromVariant(QString("%1").arg(q.value(1).toString())));
            jsonObj.insert("textDocumentStageStatusDateTime", QJsonValue::fromVariant(QString("%1").arg(q.value(2).toString())));

            jsonArr.append(jsonObj);
        }

        jsonDoc.setArray(jsonArr);
        return QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Compact));
    }
    else {
        return QString();
    }
}

//Получить количество операций по документу
int DatabaseHandler::getDocumentOperationCount(const int &documentID,
                                               const int &documentOfferUserID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int countOperations = -1;
    QString query = QString("SELECT COUNT(textDocumentStageID) "
                            "FROM EDMSBase.dbo.TextDocumentStage "
                            "WHERE (textDocumentID = %1) AND (userID <> %2) ")
            .arg(documentID)
            .arg(documentOfferUserID);

    if (q.exec(query)) {
        while (q.next()) {
            countOperations = q.value(0).toInt();
        }

        return countOperations;
    }
    else {
        return countOperations;
    }
}

//Получить количество пользователей, которым был назначен документ на подпись
int DatabaseHandler::getDocumentUserCount(const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userCount = 0;
    QString query = QString("SELECT COUNT(userTextDocumentUserID) "
                            "FROM EDMSBase.dbo.UserTextDocument "
                            "WHERE textDocumentID = %1 ")
            .arg(documentID);

    if (q.exec(query)) {
        while (q.next()) {
            userCount = q.value(0).toInt();
        }

        return userCount;
    }
    else {
        return userCount;
    }
}

//Получить количество пользователей, которые одобрили документ
int DatabaseHandler::getDocumentUserAcceptCount(const int &documentID)
{
    if (!m_db.isOpen()) {
        m_db.open();
    }

    QSqlQuery q(m_db);
    int userAcceptCount = -1;
    QString query = QString("SELECT COUNT(textDocumentStageID) "
                            "FROM EDMSBase.dbo.TextDocumentStage "
                            "WHERE (textDocumentID = %1) AND (textDocumentStageStatusID = 1) ")
            .arg(documentID);

    if (q.exec(query)) {
        while (q.next()) {
            userAcceptCount = q.value(0).toInt();
        }

        return userAcceptCount;
    }
    else {
        return userAcceptCount;
    }
}
