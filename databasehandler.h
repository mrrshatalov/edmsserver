#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#include <QObject>

#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QStandardPaths>
#include <QFile>
#include <QStringList>
#include <QDateTime>
#include <QList>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariant>

#include <QDebug>

class DatabaseHandler : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseHandler(QObject *parent = nullptr);
    ~DatabaseHandler();

public:
    void initialization();

    //Проверка наличия пользователя
    int isUserRegistered(const QString &userLogin,
                         const QString &userPassword);

    //Проверка наличия пользователя (по токену)
    int isUserRegisteredByToken(const QString &userToken);

    //Установить токен пользователя
    bool setUserToken(const int &userID,
                      const QString &userToken,
                      const int userDescriptor);

    //Получить данные пользователя
    QStringList getUserData(const int &userID);

    //Получить userID по токену пользователя
    int getUserID(const QString &userToken);

    //Получить список пользователей (кроме себя)
    QString getUserList(const QString &userToken);

    //Создание нового документа
    bool createNewDocument(const QString &userToken,
                           const QString &documentName,
                           const QString &documentShortDescription,
                           const QString &documentData,
                           const QString &chooseUsersList,
                           const QString &documentFormat);

    //Получить список дескрипторов пользователей
    QStringList getUsersDescriptor(const QString &usersID);

    //Получить список моих документов (созданных)
    QString getMyDocumentList(const QString &userToken);

    //Получить список моих документов (подпись)
    QString getMyDocumentSignatureList(const QString &userToken);

    //Отклонить документ
    bool rejectDocument(const QString &userToken,
                        const int &documentID,
                        const QString &comment);

    //Получить историю документа
    QString getDocumentHistory(const int &documentID);

    //Уточнение по документу
    bool commentDocument(const QString &userToken,
                         const int &documentID,
                         const QString &comment);

    //Принятие документа
    bool acceptDocument(const QString &userToken,
                        const int &documentID,
                        const QString &comment);

    //Получить ID пользователя, создавшего документ
    int getDocumentOfferUserID(const int &documentID);

    //Получить список пользователей по документу (кроме себя)
    QList<int> getDocumentUsers(const int &documentID);

    //Скачать документ
    QString downloadDocument(const QString &userToken,
                             const int &documentID);

    //Получить список сотрудников со статусом документа по каждому сотруднику
    QString getUsersDocumentStageData(const QString &userToken,
                                      const int &documentID);

    //Загрузить документ
    bool uploadDocument(const QString &userToken,
                        const int &documentID,
                        const QString &comment,
                        const QString &documentData,
                        const QString &documentFormat);

    //Скачать документ (по стадии готовности)
    QString downloadDocumentByStage(const QString &userToken,
                                    const int &userDocumentUploaderID,
                                    const int &documentID,
                                    const int &documentStageID);




private:
    QSqlDatabase m_db;
    QString m_dbConnectionString;

    //Получить количество операций по документу
    int getDocumentOperationCount(const int &documentID,
                                  const int &documentOfferUserID);

    //Получить количество пользователей, которым был назначен документ на подпись
    int getDocumentUserCount(const int &documentID);

    //Получить количество пользователей, которые одобрили документ
    int getDocumentUserAcceptCount(const int &documentID);

};

#endif // DATABASEHANDLER_H
