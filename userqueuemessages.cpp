#include "userqueuemessages.h"

UserQueueMessages::UserQueueMessages(const QString &messageText,
                                     QObject *parent) : QObject(parent)
{
    m_messages.clear();
    m_messages.append(messageText);
}

UserQueueMessages::~UserQueueMessages()
{
    m_messages.clear();
}

void UserQueueMessages::addMessageToQueue(const QString &messageText)
{
    m_messages.append(messageText);
}

QString UserQueueMessages::getAllQueueMessages()
{
    if (m_messages.length() != 0) {
        QString resultMessage = "newEventsNotification:[";
        for(int i = 0; i < m_messages.length(); i++) {
            resultMessage.append(m_messages.at(i));
            resultMessage.append(",");
        }

        resultMessage.chop(1);
        resultMessage = resultMessage.append("]");
        return resultMessage;
    }
    else {
        return QString();
    }
}
