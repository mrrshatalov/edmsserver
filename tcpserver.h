#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>

#include <QTcpServer>
#include <QTcpSocket>
#include <QHash>
#include <QList>

#include <QCryptographicHash>
#include <QDate>
#include <QTime>
#include <QList>

#include "coreclass.h"
#include "userconnectiondatahash.h"
#include "userqueuemessages.h"

class TCPServer : public QObject
{
    Q_OBJECT
public:
    explicit TCPServer(QObject *parent = nullptr);
    ~TCPServer();

    void start();

private slots:
    void onNewConnection();
    void clientDisconnected();
    void slotReadClient();

private:
    QTcpServer *m_server;
    quint16 m_port;

    void sendDataToClient(const int &clientDescriptor,
                          const QString &data);

    QHash<int, UserConnectionDataHash *> m_clientsHash;
    QHash<int, UserQueueMessages *> m_queueHash;
    QString m_requestToServer;
};

#endif // TCPSERVER_H
