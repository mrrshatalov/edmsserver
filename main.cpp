#include <QCoreApplication>

#include "coreclass.h"
#include "rootclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    setlocale(LC_ALL, "Russian");

    RootClass *rootClass = new RootClass();
    return a.exec();
}
