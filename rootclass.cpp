#include "rootclass.h"

#define m_databaseHandler CoreClass::instance()->databaseHandler()

RootClass::RootClass(QObject *parent) : QObject(parent)
{
    m_databaseHandler->initialization();
//    int ttt = m_databaseHandler->isUserRegistered("sidor", "sidor");
//    printf(QString::number(ttt).toLatin1().data());
//    printf("\r\n\r\n");

    m_tcpServer = new TCPServer(this);
    m_tcpServer->start();
}
