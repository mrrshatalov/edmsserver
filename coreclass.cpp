#include "coreclass.h"

CoreClass* CoreClass::m_instance = 0;

CoreClass::CoreClass()
{
    m_databaseHandler = new DatabaseHandler();
}

CoreClass::CoreClass(const CoreClass &) {}

DatabaseHandler *CoreClass::databaseHandler() const
{
    return m_databaseHandler;
}

CoreClass::~CoreClass()
{
    delete m_databaseHandler;
}

CoreClass& CoreClass::operator =(const CoreClass &)
{
    return *this;
}
