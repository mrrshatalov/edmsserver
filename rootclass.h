#ifndef ROOTCLASS_H
#define ROOTCLASS_H

#include <QObject>

#include "coreclass.h"
#include "tcpserver.h"

class RootClass : public QObject
{
    Q_OBJECT
public:
    explicit RootClass(QObject *parent = nullptr);

signals:

private:
    TCPServer *m_tcpServer;

};

#endif // ROOTCLASS_H
