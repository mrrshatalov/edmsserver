#include "tcpserver.h"

#define m_databaseHandler CoreClass::instance()->databaseHandler()

TCPServer::TCPServer(QObject *parent) : QObject(parent)
{
    m_requestToServer.clear();

    m_server = new QTcpServer(this);
    connect(m_server, SIGNAL(newConnection()),
            this, SLOT(onNewConnection()));
    m_server->setMaxPendingConnections(10);
    m_port = 8090;
}

TCPServer::~TCPServer()
{
    if (m_server->isListening()) {
        m_server->close();
    }

    m_requestToServer.clear();
    m_server->deleteLater();
}

void TCPServer::start()
{
    if (m_server->listen(QHostAddress::Any, m_port)) {
        printf("TCP is started! \n");
    }
    else {
        printf("error start TCP Server! \n");
        m_server->close();
    }
}

void TCPServer::onNewConnection()
{
    QTcpSocket* clientSocket = m_server->nextPendingConnection();
     int idusersocs = clientSocket->socketDescriptor();

//     qDebug() << clientSocket->socketDescriptor();

     m_clientsHash[idusersocs] = new UserConnectionDataHash(-1,
                                                            "",
                                                            "",
                                                            clientSocket);
     connect(m_clientsHash[idusersocs]->getClientSocket(), SIGNAL(readyRead()),
            this, SLOT(slotReadClient()));
     connect(m_clientsHash[idusersocs]->getClientSocket(), SIGNAL(disconnected()),
             this, SLOT(clientDisconnected()));
}

void TCPServer::clientDisconnected()
{
    int disconnectedClientSocketDescriptor = -1;
    foreach(int key, m_clientsHash.keys())
    {
        if (m_clientsHash.value(key)->getClientSocket()->write("1") == -1) {
           disconnectedClientSocketDescriptor = key;
           break;
        }
    }

    if (m_clientsHash.contains(disconnectedClientSocketDescriptor)) {
        m_clientsHash.remove(disconnectedClientSocketDescriptor);
    }
}

void TCPServer::slotReadClient()
{
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());

//    qDebug() << "m_client->bytesAvailable() - " << client->size() << client->bytesAvailable();
    while (!client->atEnd()) {
        if (client->bytesAvailable() > 0) {
            m_requestToServer.append(QString::fromLocal8Bit(client->readAll()));
        }
    }

    bool isAllDataRecieved = false;
    if (m_requestToServer.indexOf("{", Qt::CaseInsensitive) != -1 &&
            m_requestToServer.indexOf("}", Qt::CaseInsensitive) != -1) {

            isAllDataRecieved = true;
    }

    if (isAllDataRecieved) {
        m_requestToServer = m_requestToServer.replace("{", "").replace("}", "");
        int clientDescriptor = client->socketDescriptor();

        //Вход пользователя
        if (m_requestToServer.indexOf("userEnter",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");

            int errorCode = -1;
            QStringList userResultData = QStringList() << "" << "" << "" << "";
            QString userToken;

            if (data.length() == 3) {
                //Проверка наличия пользователя
                int userID = m_databaseHandler->isUserRegistered(data.at(1),
                                                                 data.at(2));

                if (userID != -1) {
                    userResultData = m_databaseHandler->getUserData(userID);
                    userToken = QString(QCryptographicHash::hash(QString("%1&%2&%3&%4")
                                                                 .arg(userID)
                                                                 .arg(clientDescriptor)
                                                                 .arg(QDate::currentDate().toString("dd.MM.yyyy"))
                                                                 .arg(QTime::currentTime().toString("hh:mm:ss:ms"))
                                                                 .toLocal8Bit(),
                                                                 QCryptographicHash::Md5).toHex());
                    m_databaseHandler->setUserToken(userID,
                                                    userToken,
                                                    clientDescriptor);
                    m_clientsHash[clientDescriptor]->setUserID(userID);
                    m_clientsHash[clientDescriptor]->setUserLogin(userResultData.at(0));
                    m_clientsHash[clientDescriptor]->setUserToken(userToken);

                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\","
                                         "\"userID\":%3,"
                                         "\"userName\":\"%4\","
                                         "\"userSurname\":\"%5\","
                                         "\"userPostName\":\"%6\","
                                         "\"userToken\":\"%7\"}")
                                 .arg("userEnter")
                                 .arg(errorCode)
                                 .arg(userID)
                                 .arg(userResultData.at(1))
                                 .arg(userResultData.at(2))
                                 .arg(userResultData.at(3))
                                 .arg(userToken));


                //Отправка пользователю сообщений из очереди ожидания во время его отсутствия в сети
                if (m_queueHash.contains(userID)) {
                    if (m_clientsHash[clientDescriptor]->getClientSocket()->write(m_queueHash[userID]->getAllQueueMessages().toLocal8Bit()) != -1) {
                        m_queueHash.remove(userID);
                    }
                }
                //
            }
            else {
                errorCode = 3;
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("userEnter")
                                 .arg(errorCode));
            }
        }
        //

        //Вход пользователя (при переподключении к серверу)
        if (m_requestToServer.indexOf("userReEnter",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");

            int errorCode = -1;
            QStringList userResultData = QStringList() << "" << "" << "" << "";
            QString userToken = data.at(1);

            if (data.length() == 2) {
                //Проверка наличия пользователя
                int userID = m_databaseHandler->isUserRegisteredByToken(userToken);

                if (userID != -1) {
                    userResultData = m_databaseHandler->getUserData(userID);
                    m_clientsHash[clientDescriptor]->setUserID(userID);
                    m_clientsHash[clientDescriptor]->setUserLogin(userResultData.at(0));
                    m_clientsHash[clientDescriptor]->setUserToken(userToken);

                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("userReEnter")
                                 .arg(errorCode));
            }
            else {
                errorCode = 3;
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("userReEnter")
                                 .arg(errorCode));
            }
        }
        //

        //Выход пользователя
        if (m_requestToServer.indexOf("userQuit",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = -1;

            if (data.length() == 2) {
                QString userToken = data.at(1);
                m_databaseHandler->setUserToken(m_databaseHandler->getUserID(userToken),
                                                "",
                                                -1);

                errorCode = 0;
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("userQuit")
                                 .arg(errorCode));
                client->flush();
                client->close();
            }
            else {
                errorCode = 1;
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("userQuit")
                                 .arg(errorCode));
            }
        }
        //

        //Получить список всех пользователей (кроме себя)
        if (m_requestToServer.indexOf("getUserList",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 2) {
                QString userToken = data.at(1);
                QString userList = m_databaseHandler->getUserList(userToken).replace("  ", "");

                sendDataToClient(clientDescriptor,
                                 QString("%1:%2")
                                 .arg("getUserList")
                                 .arg(userList));
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("getUserList")
                                 .arg(errorCode));
            }
        }
        //

        //Создание нового документа
        if (m_requestToServer.indexOf("createNewDocument", Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 7) {
                QString userToken = data.at(1);
                QString documentName = QByteArray::fromBase64(QString("%1").arg(data.at(2)).toLocal8Bit());
                QString documentShortDescription = QByteArray::fromBase64(QString("%1").arg(data.at(3)).toLocal8Bit());

                QString documentData;
                documentData.append(data.at(4));

                QString chooseUsersList = QByteArray::fromBase64(QString("%1").arg(data.at(5)).toLocal8Bit());
                QStringList usersID = chooseUsersList.split(",");
                QString documentFormat = QByteArray::fromBase64(QString("%1").arg(data.at(6)).toLocal8Bit());

                if (m_databaseHandler->createNewDocument(userToken,
                                                         documentName,
                                                         documentShortDescription,
                                                         documentData,
                                                         chooseUsersList,
                                                         documentFormat)) {
                    errorCode = 0;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("createNewDocument")
                                     .arg(errorCode));

                    for(int i = 0; i < usersID.length(); i++) {
                        bool isUserOnline = false;
                        foreach (int key, m_clientsHash.keys()) {
                            if (m_clientsHash.value(key)->getUserID() == QString("%1").arg(usersID.at(i)).toInt()) {
                                m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                           "\"methodName\":\"%1\"}")
                                                                                   .arg("newDocumentRecieved").toLocal8Bit());
                                isUserOnline = true;
                            }
                        }

                        if (!isUserOnline) {
                            if (m_queueHash.contains(QString("%1").arg(usersID.at(i)).toInt())) {
                                m_queueHash[QString("%1").arg(usersID.at(i)).toInt()]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                                         "\"methodName\":\"%1\"}")
                                                                                                 .arg("newDocumentRecieved"));
                            }
                            else {
                                m_queueHash[QString("%1").arg(usersID.at(i)).toInt()] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                                              "\"methodName\":\"%1\"}")
                                                                                                      .arg("newDocumentRecieved"));
                            }
                        }
                    }
                }
                else {
                    errorCode = 1;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("createNewDocument")
                                     .arg(errorCode));
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("createNewDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Получить список моих документов (созданных)
        if (m_requestToServer.indexOf("getMyDocumentList",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 2) {
                QString userToken = data.at(1);
                QString myDocumentList = m_databaseHandler->getMyDocumentList(userToken);

                sendDataToClient(clientDescriptor,
                                 QString("%1:%2")
                                 .arg("getMyDocumentList")
                                 .arg(myDocumentList));
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("getMyDocumentList")
                                 .arg(errorCode));
            }
        }
        //

        //Получить список моих документов (подпись)
        if (m_requestToServer.indexOf("getMyDocumentSignatureList",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 2) {
                QString userToken = data.at(1);
                QString myDocumentList = m_databaseHandler->getMyDocumentSignatureList(userToken);

                sendDataToClient(clientDescriptor,
                                 QString("%1:%2")
                                 .arg("getMyDocumentSignatureList")
                                 .arg(myDocumentList));
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("getMyDocumentSignatureList")
                                 .arg(errorCode));
            }
        }
        //

        //Отклонить документ
        if (m_requestToServer.indexOf("rejectDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 4) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QByteArray b;
                b.clear();
                b.append(data.at(3));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->rejectDocument(userToken,
                                                      documentID,
                                                      comment)) {
                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("rejectDocument")
                                 .arg(errorCode));

                int documentOfferUserID = m_databaseHandler->getDocumentOfferUserID(documentID);
                foreach (int key, m_clientsHash.keys()) {
                    if (m_clientsHash.value(key)->getUserID() == documentOfferUserID) {
                        m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                   "\"methodName\":\"%1\","
                                                                                   "\"documentID\":%2}")
                                                                           .arg("newDocumentRejected")
                                                                           .arg(documentID).toLocal8Bit());
                    }
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("rejectDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Отклонить документ (автором)
        if (m_requestToServer.indexOf("authorRejectDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 4) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QByteArray b;
                b.clear();
                b.append(data.at(3));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->rejectDocument(userToken,
                                                      documentID,
                                                      comment)) {
                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("authorRejectDocument")
                                 .arg(errorCode));

                QList<int> usersIDs = m_databaseHandler->getDocumentUsers(documentID);
                if (usersIDs.length() != 0) {
                    bool isUserOnline = false;
                    for(int i = 0; i < usersIDs.length(); i++) {
                        foreach (int key, m_clientsHash.keys()) {
                            if (m_clientsHash.value(key)->getUserID() == usersIDs.at(i)) {
                                m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                           "\"methodName\":\"%1\","
                                                                                           "\"documentID\":%2}")
                                                                                   .arg("authorDocumentRejected")
                                                                                   .arg(documentID).toLocal8Bit());
                                isUserOnline = true;
                            }
                        }

                        if (!isUserOnline) {
                            if (m_queueHash.contains(QString("%1").arg(usersIDs.at(i)).toInt())) {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                                                  "\"methodName\":\"%1\","
                                                                                                                  "\"documentID\":%2}")
                                                                                                          .arg("authorDocumentRejected")
                                                                                                          .arg(documentID));
                            }
                            else {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                                                       "\"methodName\":\"%1\","
                                                                                                                       "\"documentID\":%2}")
                                                                                                               .arg("authorDocumentRejected")
                                                                                                               .arg(documentID));
                            }
                        }
                    }
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("authorRejectDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Получить историю документа
        if (m_requestToServer.indexOf("getDocumentHistory",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 2) {
                int documentID = QString("%1").arg(data.at(1)).toInt();
                QString documentHistory = m_databaseHandler->getDocumentHistory(documentID);

                sendDataToClient(clientDescriptor,
                                 QString("%1:%2")
                                 .arg("getDocumentHistory")
                                 .arg(documentHistory));
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("getDocumentHistory")
                                 .arg(errorCode));
            }
        }
        //

        //Уточнение по документу
        if (m_requestToServer.indexOf("commentDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 4) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QByteArray b;
                b.clear();
                b.append(data.at(3));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->commentDocument(userToken,
                                                       documentID,
                                                       comment)) {
                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("commentDocument")
                                 .arg(errorCode));

                int documentOfferUserID = m_databaseHandler->getDocumentOfferUserID(documentID);
                bool isUserOnline = false;
                foreach (int key, m_clientsHash.keys()) {
                    if (m_clientsHash.value(key)->getUserID() == documentOfferUserID) {
                        m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                   "\"methodName\":\"%1\","
                                                                                   "\"documentID\":%2}")
                                                                           .arg("newDocumentCommented")
                                                                           .arg(documentID).toLocal8Bit());
                        isUserOnline = true;
                    }
                }

                if (!isUserOnline) {
                    if (m_queueHash.contains(documentOfferUserID)) {
                        m_queueHash[documentOfferUserID]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                    "\"methodName\":\"%1\","
                                                                                    "\"documentID\":%2}")
                                                                            .arg("newDocumentCommented")
                                                                            .arg(documentID));
                    }
                    else {
                        m_queueHash[documentOfferUserID] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                         "\"methodName\":\"%1\","
                                                                                         "\"documentID\":%2}")
                                                                                 .arg("newDocumentCommented")
                                                                                 .arg(documentID));
                    }
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("commentDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Уточнение по документу (автором)
        if (m_requestToServer.indexOf("authorCommentDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 4) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QByteArray b;
                b.clear();
                b.append(data.at(3));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->commentDocument(userToken,
                                                       documentID,
                                                       comment)) {
                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("authorCommentDocument")
                                 .arg(errorCode));

                QList<int> usersIDs = m_databaseHandler->getDocumentUsers(documentID);
                if (usersIDs.length() != 0) {
                    bool isUserOnline = false;
                    for(int i = 0; i < usersIDs.length(); i++) {
                        foreach (int key, m_clientsHash.keys()) {
                            if (m_clientsHash.value(key)->getUserID() == usersIDs.at(i)) {
                                m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                           "\"methodName\":\"%1\","
                                                                                           "\"documentID\":%2}")
                                                                                   .arg("authorDocumentCommented")
                                                                                   .arg(documentID).toLocal8Bit());
                                isUserOnline = true;
                            }
                        }

                        if (!isUserOnline) {
                            if (m_queueHash.contains(QString("%1").arg(usersIDs.at(i)).toInt())) {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                                                  "\"methodName\":\"%1\","
                                                                                                                  "\"documentID\":%2}")
                                                                                                          .arg("authorDocumentCommented")
                                                                                                          .arg(documentID));
                            }
                            else {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                                                       "\"methodName\":\"%1\","
                                                                                                                       "\"documentID\":%2}")
                                                                                                               .arg("authorDocumentCommented")
                                                                                                               .arg(documentID));
                            }
                        }
                    }
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("authorCommentDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Принятие документа
        if (m_requestToServer.indexOf("acceptDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 4) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QByteArray b;
                b.clear();
                b.append(data.at(3));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->acceptDocument(userToken,
                                                      documentID,
                                                      comment)) {
                    errorCode = 0;
                }
                else {
                    errorCode = 1;
                }

                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("acceptDocument")
                                 .arg(errorCode));

                int documentOfferUserID = m_databaseHandler->getDocumentOfferUserID(documentID);
                bool isUserOnline = false;
                foreach (int key, m_clientsHash.keys()) {
                    if (m_clientsHash.value(key)->getUserID() == documentOfferUserID) {
                        m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                   "\"methodName\":\"%1\","
                                                                                   "\"documentID\":%2}")
                                                                           .arg("newDocumentAccepted")
                                                                           .arg(documentID).toLocal8Bit());
                        isUserOnline = true;
                    }
                }

                if (!isUserOnline) {
                    if (m_queueHash.contains(documentOfferUserID)) {
                        m_queueHash[documentOfferUserID]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                    "\"methodName\":\"%1\","
                                                                                    "\"documentID\":%2}")
                                                                            .arg("newDocumentAccepted")
                                                                            .arg(documentID));
                    }
                    else {
                        m_queueHash[documentOfferUserID] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                         "\"methodName\":\"%1\","
                                                                                         "\"documentID\":%2}")
                                                                                 .arg("newDocumentAccepted")
                                                                                 .arg(documentID));
                    }
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("acceptDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Скачать документ
        if (m_requestToServer.indexOf("downloadDocument",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 3) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();
                QString documentData = m_databaseHandler->downloadDocument(userToken,
                                                                           documentID);
                if (!documentData.isEmpty()) {
                    sendDataToClient(clientDescriptor,
                                     QString("%1:%2")
                                     .arg("downloadDocument")
                                     .arg(documentData.replace("[", "").replace("]", "")));
                }
                else {
                    errorCode = 1;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("downloadDocument")
                                     .arg(errorCode));
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("downloadDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Получить список сотрудников со статусом документа по каждому сотруднику
        if (m_requestToServer.indexOf("getUsersDocumentStageData",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 3) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();
                QString usersDocumentStageData = m_databaseHandler->getUsersDocumentStageData(userToken,
                                                                                              documentID);
                if (!usersDocumentStageData.isEmpty()) {
                    sendDataToClient(clientDescriptor,
                                     QString("%1:%2")
                                     .arg("getUsersDocumentStageData")
                                     .arg(usersDocumentStageData));
                }
                else {
                    errorCode = 1;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("getUsersDocumentStageData")
                                     .arg(errorCode));
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("getUsersDocumentStageData")
                                 .arg(errorCode));
            }
        }
        //

        //Загрузка документа
        if (m_requestToServer.indexOf("uploadDocument", Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 6) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();

                QString documentData;
                documentData.append(data.at(3));

                QString documentFormat = QByteArray::fromBase64(QString("%1").arg(data.at(4)).toLocal8Bit());

                QByteArray b;
                b.clear();
                b.append(data.at(5));
                b = QByteArray::fromBase64(b);

                QString comment;
                comment.append(b);
                b.clear();

                if (m_databaseHandler->uploadDocument(userToken,
                                                      documentID,
                                                      comment,
                                                      documentData,
                                                      documentFormat)) {
                    errorCode = 0;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("uploadDocument")
                                     .arg(errorCode));

                    QList<int> usersIDs = m_databaseHandler->getDocumentUsers(documentID);
                    for(int i = 0; i < usersIDs.length(); i++) {
                        bool isUserOnline = false;
                        foreach (int key, m_clientsHash.keys()) {
                            if (m_clientsHash.value(key)->getUserID() == QString("%1").arg(usersIDs.at(i)).toInt()) {
                                m_clientsHash.value(key)->getClientSocket()->write(QString("{\"errorCode\":0,"
                                                                                           "\"methodName\":\"%1\","
                                                                                           "\"documentID\":%2}")
                                                                                   .arg("newDocumentUploaded")
                                                                                   .arg(documentID).toLocal8Bit());
                                isUserOnline = true;
                            }
                        }

                        if (!isUserOnline) {
                            if (m_queueHash.contains(QString("%1").arg(usersIDs.at(i)).toInt())) {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()]->addMessageToQueue(QString("{\"errorCode\":0,"
                                                                                                                  "\"methodName\":\"%1\","
                                                                                                                  "\"documentID\":%2}")
                                                                                                          .arg("newDocumentUploaded")
                                                                                                          .arg(documentID));
                            }
                            else {
                                m_queueHash[QString("%1").arg(usersIDs.at(i)).toInt()] = new UserQueueMessages(QString("{\"errorCode\":0,"
                                                                                                                       "\"methodName\":\"%1\","
                                                                                                                       "\"documentID\":%2}")
                                                                                                               .arg("newDocumentUploaded")
                                                                                                               .arg(documentID));
                            }
                        }
                    }
                }
                else {
                    errorCode = 1;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("uploadDocument")
                                     .arg(errorCode));
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("uploadDocument")
                                 .arg(errorCode));
            }
        }
        //

        //Скачать документ (по стадии готовности)
        if (m_requestToServer.indexOf("stageDocumentDownload",
                                    Qt::CaseInsensitive) != -1) {
            QStringList data = m_requestToServer.split("&");
            int errorCode = 1;

            if (data.length() == 5) {
                QString userToken = data.at(1);
                int documentID = QString("%1").arg(data.at(2)).toInt();
                int userUploaderDocumentID = QString("%1").arg(data.at(3)).toInt();
                int documentStageID = QString("%1").arg(data.at(4)).toInt();
                QString documentData = m_databaseHandler->downloadDocumentByStage(userToken,
                                                                                  userUploaderDocumentID,
                                                                                  documentID,
                                                                                  documentStageID);
                if (!documentData.isEmpty()) {
                    sendDataToClient(clientDescriptor,
                                     QString("%1:%2")
                                     .arg("stageDocumentDownload")
                                     .arg(documentData.replace("[", "").replace("]", "")));
                }
                else {
                    errorCode = 1;
                    sendDataToClient(clientDescriptor,
                                     QString("{\"errorCode\":%2,"
                                             "\"methodName\":\"%1\"}")
                                     .arg("stageDocumentDownload")
                                     .arg(errorCode));
                }
            }
            else {
                sendDataToClient(clientDescriptor,
                                 QString("{\"errorCode\":%2,"
                                         "\"methodName\":\"%1\"}")
                                 .arg("stageDocumentDownload")
                                 .arg(errorCode));
            }
        }
        //




        m_requestToServer.clear();
    }
}

void TCPServer::sendDataToClient(const int &clientDescriptor,
                                 const QString &data)
{
    QTcpSocket *client = m_clientsHash[clientDescriptor]->getClientSocket();
    client->write(data.toLocal8Bit());
    if (client->waitForBytesWritten()) {
        client->flush();
    }
}
