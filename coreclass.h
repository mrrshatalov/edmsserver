#ifndef CORECLASS_H
#define CORECLASS_H

#include <QObject>

#include "databasehandler.h"

class CoreClass : public QObject
{
    Q_OBJECT
public:
    static CoreClass* instance()
    {
        if (!m_instance)
            m_instance = new CoreClass;

        return m_instance;
    }

    DatabaseHandler *databaseHandler() const;

private:
    DatabaseHandler *m_databaseHandler;

    static CoreClass* m_instance;
    CoreClass();
    ~CoreClass();

    CoreClass(const CoreClass &);
    CoreClass& operator=(const CoreClass &);
};

#endif // CORECLASS_H
