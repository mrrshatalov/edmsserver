#ifndef USERQUEUEMESSAGES_H
#define USERQUEUEMESSAGES_H

#include <QObject>

#include <QString>

class UserQueueMessages : public QObject
{
    Q_OBJECT
public:
    explicit UserQueueMessages(const QString &messageText,
                               QObject *parent = nullptr);
    ~UserQueueMessages();

    void addMessageToQueue(const QString &messageText);
    QString getAllQueueMessages();

private:
    QList<QString> m_messages;

};

#endif // USERQUEUEMESSAGES_H
